from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from main.models import Main
from main.serializers import MainSerializer


@api_view(['GET', 'POST'])
def get_main(requests):
    title_filter = requests.query_params.get('title')
    data = Main.objects.all()
    if title_filter:
        data = data.filter(title__icontains=title_filter)
    serializer = MainSerializer(data, many=True)

    return Response(serializer.data)


@api_view(['POST'])
def create_main(requests):
    serializer = MainSerializer(data=requests.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
